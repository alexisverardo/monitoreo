@extends('layouts.default')
@section('content')
    <div class="container">
{{--        DF -H--}}
        <br>
        <br>
        <h3>df -h</h3>
        <br>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">FileSystem</th>
                <th scope="col">Size(B)</th>
                <th scope="col">Used(B)</th>
                <th scope="col">Avail(B)</th>
                <th scope="col">Percent</th>
                <th scope="col">MounPoint</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data['dfh']->all() as $item)
                <tr>
                    @foreach($item as $value)
                        <td>
                            {{$value}}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
{{--/////////////////////--}}
{{--        IP R--}}
        <br>
        <br>
        <h3>ip -r</h3>
        <br>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Dest</th>
                <th scope="col">Interface</th>
                <th scope="col">Proximo salto</th>
                <th scope="col">Protocolo</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data['ipr']->all() as $item)
                <tr>
                    @foreach($item as $value)
                        <td>
                            {{$value}}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
{{--/////////////////////--}}
{{--        ss -plutan --}}
        <br>
        <br>
        <h3>ss -plutan</h3>
        <br>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Netid</th>
                <th scope="col">State</th>
                <th scope="col">Local Address:Port</th>
                <th scope="col">Peer Address:Port</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data['ssplutan']->all() as $item)
                <tr>
                    @foreach($item as $value)
                        <td>
                            {{$value}}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
