<?php

namespace App\Http\Controllers;

use Illuminate\Http\Requestphp;
use Illuminate\Support\Collection;

class SnmpFunctionController extends Controller
{
    public function index() {

        $data = [
          'dfh' => $this->dfhFunction(),
          'ipr' => $this->iprFunction(),
          'ssplutan' => $this->ssPlutanFunction()
        ];

        return view('index', compact('data'));
    }


    public function dfh() {
        $arrayDisk = $this->dfhFunction();

        return response()->json($arrayDisk->jsonSerialize(), 200);
    }

    public function ipr() {
        $arrayIpr = $this->iprFunction();

        return response()->json($arrayIpr->jsonSerialize(), 200);
    }

    public function ssplutan() {
        $arraySsPlutan = $this->ssPlutanFunction();

        return response()->json($arraySsPlutan->jsonSerialize(), 200);
    }

    public function querySnmp($param){
        $sysdesc = snmprealwalk($param["host"], $param["community"], $param["oid"]);
        foreach($sysdesc as $key=>$value){
            $snmp[$key]=$value;
        }
        foreach($snmp as $key => $value){
            $varSnmp=explode("::",$key);
            $valueSnmp=explode(":",$value);
            $var=explode(".",$varSnmp[1]);
            $index=$var[1];
            $data[$var[0]][$index]=trim($valueSnmp[1]);
        }
        return $data;
    }

    public function dfhFunction() {
        $param = ["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "dsk"];
        $dataDisk = $this->querySnmp($param);

        $arrayDisk = new Collection();

        foreach($dataDisk['dskDevice'] as $disk => $value){
            $arrayDisk->push([
                'FileSystem' => $dataDisk["dskDevice"][$disk],
                'Size(B)' =>  $dataDisk["dskTotal"][$disk],
                'Used(B)' =>  $dataDisk["dskUsed"][$disk],
                'Avail(B)' => $dataDisk["dskAvail"][$disk],
                'Percent' =>  $dataDisk["dskPercent"][$disk],
                'MounPoint' => $dataDisk["dskPath"][$disk],
            ]);
        }

        return $arrayDisk;
    }

    public function iprFunction() {
        $param = ["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "ipCidrRouteTable"];
        $dataIpr = $this->querySnmp($param);
        $arrayIpr = new Collection();
        foreach($dataIpr['ipCidrRouteDest'] as $ip => $value){
            $param['oid'] = 'ifName';
            $dataInterface = $this->querySnmp($param);
            $arrayIpr->push([
                'Dest' => $dataIpr["ipCidrRouteDest"][$ip],
                'Interface' =>  $dataInterface['ifName'][(integer)$dataIpr["ipCidrRouteIfIndex"][$ip]],
                'Proximo salto' =>  $dataIpr["ipCidrRouteNextHop"][$ip],
                'Protocolo' => $dataIpr["ipCidrRouteProto"][$ip]
            ]);
        }

        return $arrayIpr;
    }

    public function ssPlutanFunction() {
        $tcpConnRemAddress = $this->querySnmp(["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "tcpConnRemAddress"]);
        $tcpConnRemPort = $this->querySnmp(["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "tcpConnRemPort"]);
        $tcpConnLocalAddress = $this->querySnmp(["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "tcpConnLocalAddress"]);
        $tcpConnLocalPort = $this->querySnmp(["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "tcpConnLocalPort"]);
        $tcpConnState = $this->querySnmp(["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "tcpConnState"]);
        $udpLocalAddress = $this->querySnmp(["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "udpLocalAddress"]);
        $udpLocalPort = $this->querySnmp(["host" => env('IP_SNMP', '127.0.0.1'), "community" => env('COMMUNITY', 'ale'),  "oid" => "udpLocalPort"]);

        $arraySsPlutan = new Collection();

        foreach ($tcpConnLocalAddress['tcpConnLocalAddress'] as $ip => $value) {
            $arraySsPlutan->push([
                'Netid' => 'tcp',
                'State' => $tcpConnState['tcpConnState'][$ip],
                'Local Address:Port' => $tcpConnLocalAddress['tcpConnLocalAddress'][$ip] . ':' . $tcpConnLocalPort['tcpConnLocalPort'][$ip],
                'Peer Address:Port' =>  $tcpConnRemAddress['tcpConnRemAddress'][$ip] . ':' . $tcpConnRemPort['tcpConnRemPort'][$ip],
            ]);
        }

        foreach ($udpLocalAddress['udpLocalAddress'] as $ip => $value) {
            $arraySsPlutan->push([
                'Netid' => 'udp',
                'State' => 'unconn',
                'Local Address:Port' => $udpLocalAddress['udpLocalAddress'][$ip] . ':' . $udpLocalPort['udpLocalPort'][$ip],
                'Peer Address:Port' =>  '0.0.0.0:*',
            ]);
        }

        return $arraySsPlutan;
    }
}
