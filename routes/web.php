<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\SnmpFunctionController;

Route::get('/', [SnmpFunctionController::class, 'index'])->name('index');

Route::get('df-h', [SnmpFunctionController::class, 'dfh']);
Route::get('ip-r', [SnmpFunctionController::class, 'ipr']);
Route::get('ss-plutan', [SnmpFunctionController::class, 'ssPlutan']);
