

## Monitoreo laravel

### instalacion
    
sudo apt install composer  
sudo apt install php7.2-gd php7.2-mbstring php7.2-xml php7.2-fpm php7.2-zip php7.2-curl php7.2-snmp  
composer install o sudo composer install  
cp .env.example .env  
abrir archivo .env 
###### en las ultimas dos variables de entorno se configura la ip del servidor snmp y la community

php artisan key:generate

### correr el servidor
php artisan serve

### entradas a api
http://url/    vista principal de tabla de los datos df -h, ip -r, ss -plutan  
http://url/df-h  df -h api  
http://url/ip-r  ip -r api  
http://url/ss-plutan  ss -plutan api  

## demo (demo instalada en maquina ubuntu vps google cloud)  
[vista principal](https://witdev-test.tk:8585).  
[api rest df -h](https://witdev-test.tk:8585/df-h).  
[api rest ip -r](https://witdev-test.tk:8585/ip-r).  
[api rest ss -plutan](https://witdev-test.tk:8585/ss-plutan).  

##### powerd by: alexis verardo
